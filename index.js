var GLOBAL_SOFTWARE_VERSION_NUMBER = 6;

var SOCKET_ADDRESS = 'https://clickoffice-socket.azurewebsites.net/device';


var fs = require('fs');

const uuidv4 = require('uuid/v4');
var device_id = uuidv4();

fs.readFile('ID', 'utf8', function(err, content) {
  if ((content==undefined)||(content=='')){
    console.log('REGISTER DEVICE');
    device_register( save_file );
  }else{
    device_id = content;
    console.log(device_id);
    socket_activate();
  }
});
var device_register = function( onSuccess ){
  var uuidv4 = require('uuid/v4');
  device_id = uuidv4();
  onSuccess(device_id);
}
var save_file = function(id){
  fs.writeFile('ID', id, function(err) {
    if(err) {
      return console.log(err);
    }else{
      socket_activate();
    }
  });
}



var pinStateLOCKED = 1;
var pinState = pinStateLOCKED;
var Gpio = require('onoff').Gpio; //require onoff to control GPIO
var DoorPin = new Gpio(21, 'out'); //declare GPIO4 an output
DoorPin.writeSync(pinState);


var getLocalIps = function(){

  var os = require('os');
  var ifaces = os.networkInterfaces();
  var returnValue = [];

  Object.keys(ifaces).forEach(function (ifname) {
    var alias = 0;

    ifaces[ifname].forEach(function (iface) {
      if ('IPv4' !== iface.family || iface.internal !== false) {
        // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
        return;
      }

      returnValue.push(iface.address);
      ++alias;
    });

  });
  return returnValue;
}

var getDeviceInfo = function(){
  var returnValue = {};

  var os = require('os');

  returnValue.version = GLOBAL_SOFTWARE_VERSION_NUMBER;
  returnValue.arch = os.arch();
  returnValue.cpu = os.cpus()[0].model;
  returnValue.type = os.type();
  returnValue.platform = os.platform();
  returnValue.hostname = os.hostname();
  returnValue.release = os.release();

  returnValue.networkInterfaces = [];

  var ifaces = os.networkInterfaces();
  Object.keys(ifaces).forEach(function (ifname) {
    var alias = 0;

    ifaces[ifname].forEach(function (iface) {
      if ('IPv4' !== iface.family || iface.internal !== false) {
        // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
        return;
      }

      if (alias >= 1) {
        // this single interface has multiple ipv4 addresses
        //console.log(ifname + ':' + alias, iface.address);
        returnValue.networkInterfaces.push({
          ifname: ifname
          , alias: alias
          , address: iface.address
        });
      } else {
        // this interface has only one ipv4 adress
        //console.log(ifname, iface.address);
        returnValue.networkInterfaces.push({
          ifname: ifname
          , address: iface.address
        });
      }
      ++alias;
    });

  });

  return returnValue;
}


var wifi_scan = function( onFinish ){
  var piWifi = require('pi-wifi');
  var returnValue = {
    code: undefined
    , message: undefined
    , data: undefined
  }

  piWifi.scan(function(err, networks) {
    if (err) {
      returnValue.code = 10;
      returnValue.message = err.message;
    }
    else {
      returnValue.code = 0;
      returnValue.message = '';
      returnValue.data = networks;
    }
    if (onFinish){
      onFinish(returnValue);
    }
  });
}

var wifi_save = function(params){
  /*
  params = {
    ssid: string value
    , psk: string value
    , onFinish: callback function
  }
  */

  var returnValue = {
    code: undefined
    , message: undefined
    , data: undefined
  }


  returnValue.code = 0;
  returnValue.message = 'Wifi settings saved';
  returnValue.data = {
    ssid: params.ssid
    , psk: params.psk
  };

  var exec = require('child_process').exec;
  var async = require('async');

  var SSID = params.ssid;
  var PSK = params.psk;

  exec('wpa_cli add_network', function(error, stdout, stderr){
    if(error !== null) {
    console.log('error: ' + error);

    returnValue.code = 10;
    returnValue.message = error;
    if (params.onFinish){
      params.onFinish(returnValue);
    }
  }
  else {
    let net_id = stdout.substr((stdout.indexOf('wlan') + 7)).trim();

    async.series([
      function(callback) {
        exec('wpa_cli set_network ' + net_id + ' ssid \'\"' + SSID + '\"\'', function(error, stdout){
          if(error) {
            console.error(error);

            returnValue.code = 11;
            returnValue.message = error;
          }
          else {
            console.log('SSID result: ' + stdout);
            callback();
          }
        });
      },
      function(callback) {
        exec('wpa_cli set_network ' + net_id + ' psk \'\"' + PSK + '\"\'', function(error, stdout){
          if(error) {
            console.error(error);

            returnValue.code = 12;
            returnValue.message = error;
          }
          else {
            console.log('PSK result: ' + stdout);
            callback();
          }
        });
      },
      function(callback) {
        exec('wpa_cli set_network ' + net_id + ' priority \'100\'', function(error, stdout){
          if(error) {
            console.error(error);

            returnValue.code = 13;
            returnValue.message = error;
          }
          else {
            console.log('Piority result: ' + stdout);
            callback();
          }
        });
      },
      function(callback) {
        exec('wpa_cli enable_network ' + net_id , function(error, stdout){
          if(error) {
            console.error(error);

            returnValue.code = 14;
            returnValue.message = error;
          }
          else {
            console.log('Enabled result: ' + stdout);
            callback();
          }
        });
      },
      function(callback) {
        exec('wpa_cli save_config', function(error, stdout){
          if(error) {
            console.error(error);

            returnValue.code = 15;
            returnValue.message = error;
          }
          else {
            console.log('select_network result: ' + stdout);
            callback();
          }
        });
      },
      /*
      function(callback) {
        exec('sudo /sbin/shutdown -r now', function(error, stdout){
          if(error) {
            console.error(error);
          }
          else {
            console.log('ShutdownProcess: ' + stdout);
            callback();
          }
        });
      }*/
    ]
    , function(errs, results) {
        if(errs) throw errs;    // errs = [err1, err2, err3]
        console.log('results: ' + results);   // results = [result1, result2, result3]

        if (params.onFinish){
          params.onFinish(returnValue);
        }
      });
    }
  });

}



var DEVICE_REBOOT = function(onFinish){

  var returnValue = {
    code: undefined
    , message: undefined
    , data: undefined
  }

  if (onFinish){
    returnValue.code = 0;
    returnValue.message = 'Restart Process Started';
    console.error('Restart Process Started');
    onFinish(returnValue);
  }

  var exec = require('child_process').exec;

  exec('sudo /sbin/shutdown -r now', function(error, stdout){
    if (error) {
      returnValue.code = 10;
      returnValue.message = error;
      console.error(error);

      if (onFinish){
        onFinish(returnValue);
      }
    }
    else {
      returnValue.code = 0;
      returnValue.message = 'Restart Process Started';
      console.error('Restart Process OK');

      /* NAO HAVERA TEMPO PARA DEVOLVER ESTE STATUS com o processo de shutdor -r -nom
      if (onFinish){
        onFinish(returnValue);
      }*/
    }
  });
}




var socket_activate = function(){
  var io = require('socket.io-client');

  var socket = io(SOCKET_ADDRESS);

  socket.on('connect', () => {
    console.log(socket.id);
    console.log(getLocalIps())

    var localIPs = getLocalIps();
    var local_address = '';
    if (localIPs.length>0){
      local_address = localIPs.join();
    }

    socket.emit('register', {device_id:device_id, local_address:local_address});
    console.log('Register')
  });

  socket.on('door lock', function(data){
    console.log('DOOR Locked ', data);
    DoorPin.writeSync(pinStateLOCKED);
  });
  socket.on('door unlock', function(data){
    console.log('DOOR Unlocked ', data);
    DoorPin.writeSync(1 - pinStateLOCKED);
  });
  socket.on('door unlock-lock', function(data){
    console.log('DOOR Unlocked and Lock after N seconds ', data);
  });


  socket.on('door locker', function(data, callback){
    var returnData = {
      error:0
      , message:'Command executed'
    };

    pinState = data.pinState;
    DoorPin.writeSync(pinState);
    console.log('Set State', pinState);
    socket.emit('door locker state',{pinState: pinState})

    if (data.toggleStateAfter){
      console.log('Toggle State after ', data.toggleStateAfter)
      setTimeout(function(){

        //pinState = 1 - pinState;
        pinState = pinStateLOCKED; /* RULE!!!!!!!!!!! ---->>>  ALWAYS LOCKED BY DEFAULT */

        DoorPin.writeSync(pinState);
        console.log('Set State', pinState);
        socket.emit('door locker state',{pinState: pinState})
      }, data.toggleStateAfter);
    }

    console.log('DOOR LOCKER', data);
    if (callback){
      callback(returnData);
      console.log('callback',returnData)
    }
  });

  socket.on('get device info', function(data, callback){
    var deviceInfo = getDeviceInfo();
    deviceInfo.device_id = device_id;
    var sendTo_socket_id = data.returnToSocketID;;

    console.log('GET DEVICE INFO', data);
    if (callback){
      var returnData = {
        error:0
        , message:'Command executed'
        , data: deviceInfo
      };

      callback(returnData);
      console.log('callback',returnData)
    } else {

      var data = {
        socket_id: sendTo_socket_id
        , command: 'device info'
        , data: deviceInfo
      }

      console.log('emit device info',data)
      /*
      socket.emit('device info', data);

      */
      socket.emit('send to Admin', data);

    }
  });

  socket.on('get wifi scan', function(data){
    console.log('GET WIFI SCAN');
    var sendTo_socket_id = data.returnToSocketID;

    wifi_scan( function(returnData){
      var data = {
        socket_id: sendTo_socket_id
        , command: 'device wifi scan'
        , data: returnData
      };
      console.log('emit device wifi scan',data)
      socket.emit('send to Admin', data);
    } );

  });

  socket.on('set wifi', function(data){
    console.log('SET WIFI');
    var sendTo_socket_id = data.returnToSocketID;
    var wifi_ssid = data.ssid;
    var wifi_psk = data.psk;

    wifi_save({
      ssid: wifi_ssid
      , psk: wifi_psk
      , onFinish: function(returnData){
        var data = {
          socket_id: sendTo_socket_id
          , command: 'device set wifi'
          , data: returnData
        };
        console.log('emit device set wifi',data)
        socket.emit('send to Admin', data);
      }
    });
  });



  socket.on('DEVICE_REBOOT', function(data){
    console.log('DEVICE_REBOOT');
    var sendTo_socket_id = data.returnToSocketID;

    DEVICE_REBOOT( function(returnData){
      var data = {
        socket_id: sendTo_socket_id
        , command: 'device DEVICE_REBOOT'
        , data: returnData
      };
      console.log('emit DEVICE_REBOOT',data)
      socket.emit('send to Admin', data);
    } );

  });


}

console.log('Device on')
console.log('Version', GLOBAL_SOFTWARE_VERSION_NUMBER)
