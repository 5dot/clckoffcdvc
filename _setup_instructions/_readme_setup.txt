Instrucoes para Instalacao e Configuracao




###################################################
# 1 ###############################################
###################################################

Verifique se os programas node e npm estao instalados
Caso nao, execute o seguinte comando:


Somente para fins de verificar a versao
[command]: node -v
[command]: npm -v



Caso nao estejam, execute o passo a seguir para instalar




###################################################
# 2 ###############################################
###################################################
Verifique a plataforma
[command]: uname -m
armv7l ---> Raspberry Pi 3 B
armv6l ---> Raspberry Pi Zero W


armv7l ---> Raspberry Pi 3 B
[command]: wget https://nodejs.org/dist/v12.13.0/node-v12.13.0-linux-armv7l.tar.xz
[command]: tar -xf node-v12.13.0-linux-armv7l.tar.xz
[command]: sudo cp -R node-v12.13.0-linux-armv7l/* /usr/local/


armv6l ---> Raspberry Pi Zero W
[command]: wget https://nodejs.org/dist/v11.15.0/node-v11.15.0-linux-armv6l.tar.gz
[command]: tar -xzf node-v11.15.0-linux-armv6l.tar.gz
[command]: sudo cp -R node-v11.15.0-linux-armv6l/* /usr/local/



Comando para remover os arquivos de instalacao
[command]: rm -rf node-v*





###################################################
# 3 Instalacao do programa GIT
###################################################

[command]: sudo apt install git






###################################################
# 4 Instalacao do programa de controle do dispositivo
###################################################

[command]: git clone https://5dot@bitbucket.org/5dot/clckoffcdvc.git

1) Renomear o diretorio para  "clickoffice"
[command]: mv clckoffcdvc/ clickoffice

2) Entrar no diretorio clickoffice
[command]: cd clickoffice/

3) Executar a instalacao do modulos
[command]: npm install



















Run on startup


[command]: sudo npm install -g pm2
Don't worry if you see skipping optional dependency messages.

Step 9. Run pm2
When you run pm2 for the first time it needs to initialize.

Run the command to display a status report:

[command]: pm2 status
Because pm2 needs to start first, you will see a logo, some instructions and a message like this:

[PM2] Spawning PM2 daemon with pm2_home=/home/pi/.pm2
[PM2] PM2 Successfully daemonized
Followed by a status report.

If you run that command again, now that pm2 is running, you will just see the status report.

Step 10. Run the app under pm2
You can run a NodeJS app, Python app or even a bash script under pm2. It will monitor your app or script to make sure it is running. If the app unexpectedly stops, pm2 will attempt to restart it.

[command]: cd ~/clickoffice/
[command]: pm2 start index.js
The start command will also trigger the status command, where you should see that your app (hello) is running.

Step 11. Save and resurrect apps
If you reboot your Pi, the services you setup will be lost. You can at least save them with this command:

[command]: pm2 save
If you do reboot, you can restart saved pm2 apps by using this command:

[command]: pm2 resurrect
Step 12. Run on startup
After a reboot, you don't want to have to remote login to the server and restart apps all the time. Fortunately you can setup a service to startup pm2 when the Pi is rebooted. Behind the scenes on startup the pm2 service will resurrect anything saved.

To setup pm2 to automatically start when your Pi reboots, run this command:

[command]: pm2 startup
You will see output with instructions. You still have to follow the instructions to make pm2 start automatically on reboot:

[PM2] Init System found: systemd
[PM2] To setup the Startup Script, copy/paste the following command:
sudo env PATH=$PATH:/usr/local/bin /usr/local/lib/node_modules/pm2/bin/pm2 startup systemd -u pi --hp /home/pi
Copy the last command line to the clipboard, then run it:

[command]: sudo env PATH=$PATH:/usr/local/bin /usr/local/lib/node_modules/pm2/bin/pm2 startup systemd -u pi --hp /home/pi
To test it, reboot the Pi with this command:

[command]: sudo reboot
