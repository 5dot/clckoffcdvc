
require('simple-git')()
  .exec(() => console.log('Starting pull...'))
  .pull('origin', 'master', {'--no-rebase': null}, (err, update) => {
    if(update && update.summary.changes) {
      console.log('NEED UPDATE')
      require('child_process').exec('npm restart');
    }
  })
  .exec(() => console.log('pull done.'));
